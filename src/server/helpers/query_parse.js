const maker = (data, filter) => {
  if(!data){
    return filter;
  }

  let i, match
  for(i in data){
    if(!data[i].length){
      continue;
    }

    match = {
      $or: data[i].map((f) => {
        let where = {}
        where[i] = f.value;
        return where;
      })
    }

    filter.$and.push(match);
  }

  return filter;
};

const origin = (data, filter) => {
  if(data.origin && data.origin.length){
    filter.origin = {$in: data.origin}
  }

  return filter;
};

const queryParse = (filter) => {
  filter = JSON.parse(filter);
  
  let query = {$and: []};
  query = maker(filter.exam, query);
  query = maker(filter.clinical_notes, query);
  query = origin(filter, query)


  if(filter.type === 'graph' && filter.bounds){
    query.$and.push({
      'patient_address.geo': {
        $geoWithin: {
          $box: [[filter.bounds._southWest.lat, filter.bounds._southWest.lng], [filter.bounds._northEast.lat, filter.bounds._northEast.lng]]
        }
      }
    });
  }

  if(!query.$and.length){
    delete(query.$and);
  }

  return query;
};

module.exports = queryParse;