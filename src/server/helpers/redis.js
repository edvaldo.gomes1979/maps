const client = require('../config/redis');
const md5   = require('md5');


class Redis{
  static key(key){
    return md5(typeof(key) === 'string' ? key : JSON.stringify(key));
  }

  static async get(key){
    let data = await client.get(key);
    return data ? JSON.parse(data) : data;
  }

  static async set(key, data){
    return client.set(key, JSON.stringify(data));
  }
}

module.exports = Redis;