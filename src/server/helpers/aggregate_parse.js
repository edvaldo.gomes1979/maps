const config = require('../config/filter');


const aggregateParseMatch = (aggregate, options) => {
  let match = options.filter;  
  match.origin = options.origin;

  aggregate.push({$match: match});
  return aggregate;
};

const aggregateParseSum = (condition, field) => {
  let sum  = {},
      keys = Object.keys(condition.value),
      and  = [];

  keys.forEach((key) => {
    let item = {};
    item[key] = [field, condition.value[key]];

    and.push(item);
  });

  return {$sum: sum};
};

const aggregateParseGroup = (aggregate, options) => {
  let group = {
    _id: options.origin
  };

  options.exam.options.forEach((condition) => {
    group[condition.label] = aggregateParseSum(condition, options.exam.name);
  });
  
  aggregate.push({$group: group});
  return aggregate;
};

const aggregateParse = (options) => {
  options[exam] = config[options.exam];

  let aggregate = [];

  aggregate = aggregateParseMatch(aggregate, options);
  aggregate = aggregateParseGroup(aggregate, options);

  return aggregate;
};