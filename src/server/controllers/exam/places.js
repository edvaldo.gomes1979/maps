const place = require('../../models/place');

class ExamPlacesController{

  // GET /api/exam/places
  static async index(request, response){
    let places = await place.all();
    response.send(places);
  }


}

module.exports = ExamPlacesController;