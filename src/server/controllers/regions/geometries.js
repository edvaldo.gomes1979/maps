const queryParseHelper  = require('../../helpers/query_parse');

const Region = require('../../models/region');
const Exam   = require('../../models/exam');

class RegionsGeometriesController{
  // GET /regions/geometries
  static async index(request, reponse){


    let filter  = queryParseHelper(request.query.filter);
    let regions = request.query.regions;

    if(regions === 'total'){
      let total = await Exam.total({filter: filter});
      return reponse.send({total: total});
    }

    Region.all({"_id": {$in: regions}}, filter, (regions) => {
      reponse.send({regions: regions});
    });

  }
}

module.exports = RegionsGeometriesController;