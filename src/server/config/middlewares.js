let path = require('path'),
    fs   = require('fs');


module.exports = (app) => {
  let middlewares, file;

  file = path.resolve(__dirname, '..', 'middlewares');
  middlewares = fs.readdirSync(file);

  middlewares.forEach((middleware) => {
    if(!/\.js$/.test(middleware)){
      return;
    }

    app = require(path.resolve(file, middleware))(app);
  });
};