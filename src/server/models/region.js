const async = require('async');
const redis = require('../helpers/redis');

const RegionSchema = require('../schema/region');
const Exam         = require('./exam');

class Region{
   
  static async all(filter = {}, densityFilter = {}, cb){
    let regions = await this._all(filter);
    let i, total, reponse = [];

    async.each(regions, async (item) => {
      let where = Object.assign({}, densityFilter);

      where['patient_address.geo2'] = {
        $geoWithin: {
          $geometry: this.geometry(item)
        }
      };

      total = await Exam.total({filter: where});
      item = {
        type: 'Feature',
        id:   item._id,
        properties: {
          name: item.name,
          density: total
        },
        geometry: item.geometry
      };

      if(total > 0){
        reponse.push(item);
      }
    }, () => {
      cb(reponse);
    });
  }

  static async _all(filter){
    let cache = await this._getCache(filter, 'total');

    if(cache){
      return cache;
    }

    let total = await RegionSchema.find(filter).exec();

    console.log(filter);
    console.log(total);
    this._setCache(filter, total, 'total');  

    return total; 
  }

  static geometry(region){
    return region.geometry;
    region = JSON.parse(JSON.stringify(region));
    region.geometry.coordinates = region.geometry.coordinates.map((coordinates) => {
      return coordinates.map((item) => {
        return item.sort();
      });
    });

    return region.geometry;
  }

  static async _getCache(options, namespace = 'regions'){
    let key = `${namespace}:${redis.key(options)}`;
    return await redis.get(key);
  }

  static async _setCache(options, data, namespace = 'regions'){
    let key = `${namespace}:${redis.key(options)}`;
    redis.set(key, data);
  }
}

module.exports = Region;