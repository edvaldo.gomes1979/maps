const config = require('../config/config');

module.exports = (app) => {
  app.use((request, response, next) => {
    if(config.security.authorization_key !== request.headers.authorization){
      return response.send({error: 'Unauthorized'}, 401);
    }

    next();
  });

  return app;
};