const config = require('../config/config');

module.exports = (app) => {
  app.use((request, response, next) => {
    response.set('Cache-Control', 'public, max-age=31557600, s-maxage=31557600'); 
    next();
  });

  return app;
};