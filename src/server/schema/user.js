const mongoose = require("mongoose");

//Employee Model without any fixed schema
const UserSchema = new mongoose.Schema({
    _id:          mongoose.Schema.Types.ObjectId,
    name:         mongoose.Schema.Types.String,
    email:        mongoose.Schema.Types.String,
    google_id:    mongoose.Schema.Types.String,
    access_token: mongoose.Schema.Types.String,
    expires_at:   mongoose.Schema.Types.String,
    tokenId:      mongoose.Schema.Types.String,
    avatar:       mongoose.Schema.Types.String,
    last_access:  mongoose.Schema.Types.Date,
    access_count: mongoose.Schema.Types.Number
  },
  {strict:false, collection: 'users'}
);

module.exports = mongoose.model('User', UserSchema);