const mongoose = require("mongoose");

//Employee Model without any fixed schema
const PlaceSchema = new mongoose.Schema({
    name:    mongoose.Schema.Types.String,
    lat:     mongoose.Schema.Types.String,
    lon:     mongoose.Schema.Types.String,
    address: mongoose.Schema.Types.String
  },
  {strict:false, collection: 'places'}
);

module.exports = mongoose.model('Place', PlaceSchema);