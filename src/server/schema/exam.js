const mongoose = require("mongoose");

//Employee Model without any fixed schema
const ExamSchema = new mongoose.Schema({
    geo:  mongoose.Schema.Types.Array,
    geo2: mongoose.Schema.Types.Mixed

  },
  // {strict:false, collection: 'patient_group_results'}
  {strict:false, collection: 'patients'}
);

module.exports = mongoose.model('Exam', ExamSchema);